package ver2.snik2004.com.switter;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import ver2.snik2004.com.interfaces.AuthentificationListener;
import ver2.snik2004.com.switter.Constans;
import ver2.snik2004.com.switter.R;

public class AuthentificationDialog extends Dialog {
    private AuthentificationListener listener;
    private Context context;
    private WebView webView;
    private final String url = Constans.BASE_URL
            +"?client_id="
            +Constans.INSTAGRAM_CLIENT_ID
            +"&redirect_uri="
            +Constans.REDIRECT_URL
            +"&response_type=token"
            +"&display=touch&scope=public_content";

    public AuthentificationDialog(Context context, AuthentificationListener listener){
        super(context);
        this.context = context;
        this.listener = listener;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setContentView(R.layout.web_view_instagram);
        initializeWebView();
    }

    private void initializeWebView() {
        webView = (WebView) findViewById(R.id.webWiew);
        webView.loadUrl(url);
        webView.setWebViewClient(new WebViewClient(){
            String access_token;
            boolean authComplete;
            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                super.onPageStarted(view, url, favicon);
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
                if (url.contains("#access_token=")&&!authComplete){
                    Uri uri = Uri.parse(url);
                    access_token = uri.getEncodedFragment();
                    access_token = access_token.substring(access_token.lastIndexOf("=")+1);
                    Log.e("access_token", access_token);
                    authComplete = true;
                    listener.onCodeReceived(access_token);
                    dismiss();
                }else if (url.contains("?error")){
                    Log.e("access_token","getting error fetching access token");
                    Log.e("response", url);
                    dismiss();
                }
            }
        });
        webView.getSettings().setJavaScriptEnabled(true);
        webView.loadUrl(url);
    }
}
