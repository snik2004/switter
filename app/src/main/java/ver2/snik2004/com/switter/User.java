package ver2.snik2004.com.switter;

public class User {
    private String profile_picture;
    private String full_name;
    private String bio;


    public String getBio() {
        return bio;
    }


    public String getProfile_picture() {
        return profile_picture;
    }


    public String getFull_name() {

        return full_name;
    }
}
