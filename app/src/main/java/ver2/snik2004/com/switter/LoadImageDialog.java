package ver2.snik2004.com.switter;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.webkit.WebView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;

import ver2.snik2004.com.interfaces.AuthentificationListener;

import static ver2.snik2004.com.switter.Constans.GET_IMAGE_URL;
import static ver2.snik2004.com.switter.Constans.INSTAGRAM_CLIENT_ID_SECRET;

public class LoadImageDialog extends Dialog {
    private AuthentificationListener listener;
    private Context context;
    private WebView webView;
    private String urlString= GET_IMAGE_URL+INSTAGRAM_CLIENT_ID_SECRET;

    public LoadImageDialog(Context context, AuthentificationListener listener) throws IOException, JSONException {
        super(context);
        this.context = context;
        this.listener = listener;
    }

    private URL url;
    InputStream inputStream= url.openConnection().getInputStream();
    private LoadImageDialog instagramImpl;
    String response= instagramImpl.streamToString(inputStream);
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setContentView(R.layout.web_view_instagram);
    }
    public String streamToString(InputStream is) throws IOException, JSONException {
        String string="";
        JSONObject jsonObject=(JSONObject)new JSONTokener(response).nextValue();
        try {
            JSONArray jsonArray= jsonObject.getJSONArray("data");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        if(is!=null){
            StringBuilder stringBuilder=new StringBuilder();
            String line;
            try{
                BufferedReader reader=new BufferedReader(
                        new InputStreamReader(is));

                while((line= reader.readLine())!=null){
                    stringBuilder.append(line);
                }

                reader.close();
            } finally{
                is.close();
            }

            string= stringBuilder.toString();
        }

        return string;
    }

}
