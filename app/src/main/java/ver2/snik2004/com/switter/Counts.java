package ver2.snik2004.com.switter;

public class Counts {
    private int media;
    private int follows;
    private int followed_by;

    public int getMedia() {
        return media;
    }

    public int getFollows() {
        return follows;
    }

    public int getFollowed_by() {
        return followed_by;
    }
}
