package ver2.snik2004.com.switter;

import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import ver2.snik2004.com.interfaces.AuthentificationListener;

public class MainActivity extends AppCompatActivity implements AuthentificationListener {
    private Toolbar toolbar;
    Button btn_login;
    private AuthentificationDialog auth_dialog;
    SharedPreferences prefs = null;
    String token = null;
    String token_counts = null;
    TextView tv_name = null;
    TextView tv_full_name = null;
    TextView tv_scope_posts = null;

    TextView bio_tv = null;
    TextView is_business_tv = null;
    TextView counts_tv = null;
    TextView tv_scope_followers = null;
    TextView tv_scope_following = null;
    ImageView profile_picture = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btn_login = (Button) findViewById(R.id.login);
        tv_name = (TextView) findViewById(R.id.username);
        tv_full_name = (TextView) findViewById(R.id.full_name_tv);
        bio_tv = (TextView) findViewById(R.id.bio_tv);
        counts_tv = (TextView) findViewById(R.id.counts_tv);
        is_business_tv = (TextView) findViewById(R.id.is_business_tv);
        profile_picture = (ImageView) findViewById(R.id.profile_picture);
        tv_scope_posts = (TextView) findViewById(R.id.scope_posts);
        tv_scope_followers = (TextView) findViewById(R.id.scope_folowers);
        tv_scope_following = (TextView) findViewById(R.id.scope_folowing);
        prefs = getSharedPreferences(Constans.PREF_NAME, MODE_PRIVATE);
        token = prefs.getString("token", null);
        if (token != null) {
            btn_login.setText("Logout");
            getUserInfoByAccessToken(token);
        } else {
            btn_login.setText("Instagram Login");
        }




/*        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        BottomNavigationView bottomNav = findViewById(R.id.navigation);
        bottomNav.setOnNavigationItemSelectedListener(navListener);
        getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new HomeFragment()).commit();

        btn1 = (Button) findViewById(R.id.btn1);
        btn1.setOnClickListener(this);*/
    }

    private void getUserInfoByAccessToken(String token) {
        new RequestInstagramAPI().execute();
    }

    private class RequestInstagramAPI extends AsyncTask<Void, String, String> {

        @Override
        protected String doInBackground(Void... params) {
            HttpClient httpClient = new DefaultHttpClient();
            HttpGet httpGet = new HttpGet(Constans.GET_USER_UNFO_URL + token);
            try {
                HttpResponse response = httpClient.execute(httpGet);
                HttpEntity httpEntity = response.getEntity();
                String json = EntityUtils.toString(httpEntity);
                return json;
            } catch (ClientProtocolException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(String response) {
            super.onPostExecute(response);
            if (response != null) {
                try {
                    JSONObject json = new JSONObject(response);
                    Log.e("response", json.toString());
                    JSONObject jsonData = json.getJSONObject("data");
                    JSONObject jsonCounts = jsonData.getJSONObject("counts");
                    if (jsonData.has("id")) {
                        String id = jsonData.getString("id");
                        SharedPreferences.Editor editor = prefs.edit();
                        editor.putString("userId", id);
                        editor.apply();

                        String user_name = jsonData.getString("username");
                        String full_name = jsonData.getString("full_name");
                        String bio = jsonData.getString("bio");
                        String profile_pic = jsonData.getString("profile_picture");
                        String scope_posts = jsonCounts.getString("media");
                        String scope_following = jsonCounts.getString("follows");
                        String scope_followers = jsonCounts.getString("followed_by");

                        tv_name.setText(user_name);
                        tv_full_name.setText(full_name);
                        Picasso.with(MainActivity.this).load(profile_pic).into(profile_picture);
                        bio_tv.setText(bio);
                        tv_scope_posts.setText(scope_posts);
                        tv_scope_followers.setText(scope_followers);
                        tv_scope_following.setText(scope_following);

                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    /*
        private BottomNavigationView.OnNavigationItemSelectedListener navListener = new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                Fragment selectedFragment = null;
                switch (item.getItemId()) {
                    case R.id.navigation_home:
                        selectedFragment = new HomeFragment();
                        break;
                    case R.id.navigation_search:
                        selectedFragment = new SearchFragment();
                        break;
                    case R.id.navigation_notifications:
                        selectedFragment = new NotificationsFragment();
                        break;
                    case R.id.navigation_message:
                        selectedFragment = new MessageFragment();
                        break;
                    case R.id.navigation_profile:
                        selectedFragment = new ProfileFragment();
                        break;
                }
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, selectedFragment).commit();
                return true;
            }

        };


        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.btn1:
                    Intent intent = new Intent(MainActivity.this, AuthorizationActivity.class);
                    startActivity(intent);
                    break;
                default:
                    break;
            }

        }*/
    @Override
    public void onCodeReceived(String auth_token) {
        if (auth_token == null)
            return;
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString("token", auth_token);
        editor.apply();
        token = auth_token;
        btn_login.setText("Logout");


    }

    public void after_click_login(View view) {
        if (token != null) {
            SharedPreferences.Editor editor = prefs.edit();
            editor.clear();
            editor.apply();
            btn_login.setText("Instagram Login");
            token = null;
        } else {


            auth_dialog = new AuthentificationDialog(this, this);
            auth_dialog.setCancelable(true);
            auth_dialog.show();
        }

    }
}
